package ca.tictocproduction.bubblefall.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.*;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;

import ca.tictocproduction.bubblefall.base.BaseScene;
import ca.tictocproduction.bubblefall.extra.GameOverWindow;
import ca.tictocproduction.bubblefall.game.GameActivity;
import ca.tictocproduction.bubblefall.manager.FacebookManager;
import ca.tictocproduction.bubblefall.manager.ResourcesManager;
import ca.tictocproduction.bubblefall.manager.SceneManager;
import ca.tictocproduction.bubblefall.manager.SceneManager.SceneType;
import ca.tictocproduction.bubblefall.object.Bird;
import ca.tictocproduction.bubblefall.object.Bubble;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.facebook.android.Facebook;

public class GameScene extends BaseScene implements IOnSceneTouchListener{ 

	// Static variables
	private static final float GAME_OVER_TEXT_X = 100;
	private static final float GAME_OVER_TEXT_Y = 570;
	private static final float SCORE_TEXT_X = 140;
	private static final float SCORE_TEXT_Y = 680;
	private static final float REPLAY_BUTTON_X = 130;
	private static final float REPLAY_BUTTON_Y = 150;
	public static final int GRAVITY = 10;
	public static final float BUBBLE_INIT_X = 240;
	public static final float BUBBLE_INIT_Y = 500;

	// HUD
	private HUD gameHUD;
	private Text scoreText;
	private Text distanceText;
	private int score = 0;
	private ButtonSprite replayButton;
	private ButtonSprite facebookButton;

	// World variables
	private PhysicsWorld physicsWorld;
	private Bubble bubble;
	private int fallingDistance=0;

	// Gameover Panel
	private boolean gameOverDisplayed = false;
	private GameOverWindow gameOverWindow;

	private TimerHandler timeHandler;

	@Override
	public void createScene() {
		createBackground();
		createHUD();
		createPhysics();
		createTimeHandler();
		createBubble();
		createGameOverComponents();
		setOnSceneTouchListener(this);
	}

	@Override
	public void disposeScene() {
		camera.setHUD(null);
		camera.setCenter(GameActivity.SCREEN_CENTER_X, GameActivity.SCREEN_CENTER_Y);
		camera.setChaseEntity(null);
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAME;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if (pSceneTouchEvent.isActionDown() && !gameOverDisplayed)
		{
			bubble.jump();
			score();
		}
		return false;
	}

	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().loadMenuScene(engine);
	}

	////////////////////////////////////////
	/// Private function
	////////////////////////////////////////
	private void createBackground()
	{
		attachChild(new Sprite(GameActivity.SCREEN_CENTER_X, GameActivity.SCREEN_CENTER_Y, resourcesManager.game_background_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});
	}

	private void createHUD()
	{
		gameHUD = new HUD();
		// CREATE SCORE TEXT
		scoreText = new Text(SCORE_TEXT_X, SCORE_TEXT_Y, resourcesManager.font, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setAnchorCenter(0, 0);    
		scoreText.setText("Score: 0");
		scoreText.setColor(Color.YELLOW);
		gameHUD.attachChild(scoreText);
		camera.setHUD(gameHUD);

		// CREATE DISTANCE TEXT
		distanceText = new Text(SCORE_TEXT_X - 40, SCORE_TEXT_Y - 50, resourcesManager.font, "Distance: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		distanceText.setAnchorCenter(0, 0);
		distanceText.setText("Distance: 0");
		distanceText.setColor(Color.YELLOW);
		attachChild(distanceText);

	}

	private void createTimeHandler(){
		timeHandler = new TimerHandler(0.9f, new ITimerCallback()
		{                                    
			public void onTimePassed(final TimerHandler pTimerHandler)
			{
				pTimerHandler.reset();
				if (!gameOverDisplayed){
					generateBird();
					timeHandler = pTimerHandler;	
				}
			}
		});
		engine.registerUpdateHandler(timeHandler);
	}

	private void createPhysics()
	{
		physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0, -GRAVITY), false); 
		physicsWorld.setContactListener(contactListener());
		registerUpdateHandler(physicsWorld);
	}

	private void createBubble()
	{
		bubble = new Bubble(BUBBLE_INIT_X, BUBBLE_INIT_Y, vbom, camera, physicsWorld){
			@Override
			public void onDie() {
				if (!gameOverDisplayed)
				{
					ResourcesManager.getInstance().bubblePop.play();
					displayGameOverPanel();
					bubble.setVisible(false);
				}
			}

			@Override
			public void scoring() {
				if (!gameOverDisplayed){
					fallingDistance = (int)getFallingDistance();
					distanceText.setText("Distance: " + fallingDistance);	
				}
			}
		};
		bubble.setCullingEnabled(true);
		attachChild(bubble);
	}

	private void createGameOverComponents() {
		// Create button replay
		replayButton = new ButtonSprite(REPLAY_BUTTON_X, REPLAY_BUTTON_Y, ResourcesManager.getInstance().gameover_replay_region, vbom);
		replayButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(ButtonSprite pButtonSprite,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				camera.getHUD().setVisible(false);
				SceneManager.getInstance().reloadGameScene(engine);
			}
		});
		replayButton.setVisible(false);
		replayButton.setScale(1.2f);

		// Create facebook button
		facebookButton = new ButtonSprite(REPLAY_BUTTON_X + 200, REPLAY_BUTTON_Y, ResourcesManager.getInstance().share_fb_region, vbom);
		facebookButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				// TODO Auto-generated method stub
				//FacebookManager.postScore("100", "Testing mode");
			}
		});
		facebookButton.setVisible(false);
		facebookButton.setScale(4.1f);


		gameOverWindow = new GameOverWindow(vbom);
	}

	private void generateBird(){
		// Generate random position for the bird
		float upper = 600f;
		float lower = 100f;
		float y = (float)(Math.random()) * (upper - lower) + lower;
		float x = (float)(Math.random());
		if ( x > 0.5) {// From right to left
			x = 460;
		}
		else {// From left to right
			x = -10;
		}

		Bird bird = new Bird(x, y, vbom, camera, physicsWorld, ResourcesManager.getInstance().bird_region){
			@Override
			public void onDie() {
				setVisible(false);
				detachSelf();
			}
		};
		//bird.setScale(0.6f);
		bird.setCullingEnabled(true);
		attachChild(bird);
		bird.move();
	}

	private void score(){
		score += fallingDistance/100;
		if (fallingDistance/100 > 0){
			distanceText.setColor(Color.RED);
			scoreText.setColor(Color.RED);
			scoreText.setText("Score: " + score + "  +" + fallingDistance/100);
		}
		else{
			distanceText.setColor(Color.YELLOW);

			scoreText.setColor(Color.YELLOW);
			scoreText.setText("Score: " + score);
		}
		fallingDistance = 0;
	}

	private void displayGameOverPanel(){
		//this.detachChild(distanceText);

		if (fallingDistance >= 100)
			score();

		gameOverWindow.display(score, GameScene.this, camera, vbom);
		gameOverDisplayed = true;

		// Display gameover HUD
		scoreText.setText("Game over");
		scoreText.setX(GAME_OVER_TEXT_X);
		scoreText.setY(GAME_OVER_TEXT_Y);

		replayButton.setVisible(true);
		this.registerTouchArea(replayButton);
		this.attachChild(replayButton);

		facebookButton.setVisible(true);
		this.registerTouchArea(facebookButton);
		this.attachChild(facebookButton);

		this.setTouchAreaBindingOnActionDownEnabled(true);
	}

	///////////////////////////////////////////////////
	/// Private Class 
	///////////////////////////////////////////////////
	private ContactListener contactListener()
	{
		ContactListener contactListener = new ContactListener()
		{
			public void beginContact(Contact contact)
			{
				final Fixture x1 = contact.getFixtureA();
				final Fixture x2 = contact.getFixtureB();

				if (x1.getBody().getUserData() != null && x2.getBody().getUserData() != null)
				{
					if (x1.getBody().getUserData().equals("bubble") || x2.getBody().getUserData().equals("bubble"))
					{
						bubble.onDie();
						x1.getBody().setType(BodyType.StaticBody);
						x2.getBody().setType(BodyType.StaticBody);
						engine.unregisterUpdateHandler(timeHandler);
					}
				} 
			}

			public void endContact(Contact contact)
			{
			}

			@Override
			public void preSolve(Contact contact,
					com.badlogic.gdx.physics.box2d.Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact,
					com.badlogic.gdx.physics.box2d.ContactImpulse impulse) {
			}
		};
		return contactListener;
	}
}
