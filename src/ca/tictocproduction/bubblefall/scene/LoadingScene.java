package ca.tictocproduction.bubblefall.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import ca.tictocproduction.bubblefall.base.BaseScene;
import ca.tictocproduction.bubblefall.game.GameActivity;
import ca.tictocproduction.bubblefall.manager.SceneManager.SceneType;

public class LoadingScene extends BaseScene{

	@Override
    public void createScene()
    {
        setBackground(new Background(Color.WHITE));
        attachChild(new Text(GameActivity.SCREEN_CENTER_X, GameActivity.SCREEN_CENTER_Y, resourcesManager.font, "Loading...", vbom));
    }

    @Override
    public void onBackKeyPressed()
    {
        return;
    }

    @Override
    public SceneType getSceneType()
    {
        return SceneType.SCENE_LOADING;
    }

    @Override
    public void disposeScene()
    {

    }
}
