package ca.tictocproduction.bubblefall.scene;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.color.Color;

import ca.tictocproduction.bubblefall.base.BaseScene;
import ca.tictocproduction.bubblefall.game.GameActivity;
import ca.tictocproduction.bubblefall.manager.SceneManager.SceneType;

public class SplashScene extends BaseScene{
	private Sprite splash;
	
	@Override
	public void createScene() {
		splash = new Sprite(0, 0, resourcesManager.splash_region, vbom){
			@Override
		    protected void preDraw(GLState pGLState, Camera pCamera) 
		    {
		       super.preDraw(pGLState, pCamera);
		       pGLState.enableDither();
		    }
		};
		setBackground(new Background(Color.WHITE));
		splash.setScale(1.5f);
		splash.setPosition(GameActivity.SCREEN_CENTER_X, GameActivity.SCREEN_CENTER_Y);
		attachChild(splash);
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_SPLASH;
	}

	@Override
	public void disposeScene() {
		splash.detachSelf();
		splash.dispose();
		this.detachSelf();
		this.dispose();
	}

}
