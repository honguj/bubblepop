package ca.tictocproduction.bubblefall.manager;


import java.io.IOException;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.content.SharedPreferences;
import android.graphics.Color;
import ca.tictocproduction.bubblefall.game.GameActivity;

public class ResourcesManager {

	public static final String PREFS_NAME = "FreeFallPref";
	private static final ResourcesManager INSTANCE = new ResourcesManager();
	private static int bestScore;

	public Engine engine;
	public GameActivity activity;
	public BoundCamera camera;
	public VertexBufferObjectManager vbom;

	//---------------------------------------------
	// TEXTURES & TEXTURE REGIONS
	//---------------------------------------------
	public ITextureRegion splash_region;
	private BitmapTextureAtlas splashTextureAtlas;

	public ITextureRegion menu_background_region;
	public ITextureRegion play_region;
	private BuildableBitmapTextureAtlas menuTextureAtlas;
	public Font font;

	// Game textures
	private BuildableBitmapTextureAtlas gameTextureAtlas;
	public ITextureRegion game_background_region;
	public ITiledTextureRegion bubble_region;
	public ITiledTextureRegion bird_region;

	// GameOver Window textures
	private BuildableBitmapTextureAtlas gameoverTextureAtlas;
	public ITextureRegion gameover_texture_region;
	public ITextureRegion gameover_window_region;
	public ITextureRegion gameover_replay_region;
	public ITextureRegion share_fb_region;

	// Sound
	public Sound bubblePop;
	public Sound bubbleJump;


	// CLASS LOGIC
	public void loadMenuResources(){
		loadMenuGraphics();
		loadMenuAudio();
		loadMenuFonts();
	}

	public void loadGameResources(){
		loadGameGraphics();
		loadGameFonts();
		loadGameAudio();
	}

	public void loadSplashScreen(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 400, 400, TextureOptions.BILINEAR);
		splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png", 0, 0);
		splashTextureAtlas.load();
	}

	public void unloadSplashScreen(){
		splashTextureAtlas.unload();
		splash_region = null;
	}

	public void loadMenuTextures(){
		menuTextureAtlas.load();
	}

	public void unloadMenuTextures(){
		menuTextureAtlas.unload();
	}

	public void unloadGameTextures(){
		gameTextureAtlas.unload();
	}

	public void loadGameoverTextures(){
		gameoverTextureAtlas.load();
	}

	public void unloadGameoverTextures(){
		gameoverTextureAtlas.unload();
	}

	private void loadMenuGraphics(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "instruction_background.png");
		play_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "button.png");

		try 
		{
			this.menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.menuTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}
	}

	private void loadMenuAudio(){

	}

	private void loadMenuFonts(){
		FontFactory.setAssetBasePath("font/");
		final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "FuturaBold.ttf", 50, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		font.load();
	}

	private void loadGameGraphics(){
		// Restore preferences
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
		bestScore = settings.getInt("bs", 0);

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		gameTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 2048, 2048, TextureOptions.BILINEAR);
		game_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "background_game.png");
		bubble_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameTextureAtlas, activity, "bubble.png", 1, 1);
		bird_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameTextureAtlas, activity, "bird_thao_1_animations.png", 3, 2);
		gameover_window_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "TransparentPanel.png");
		gameover_replay_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "play_icon_button.png");
		share_fb_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "fb_share_button.png");

		try 
		{
			this.gameTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.gameTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}
	}

	private void loadGameFonts(){

	}

	private void loadGameAudio(){
		SoundFactory.setAssetBasePath("sound/");
		try
		{
			bubblePop = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "cork.ogg");
			bubblePop.setVolume(1f);
			bubbleJump = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, "cartoonJump.ogg");
			bubbleJump.setVolume(0.2f);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveNewScore(int newScore){
		if (newScore > bestScore){
			SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putInt("bs", newScore);

			// Commit the edits!
			editor.commit();
		}
	}

	public int getBestScore(){
		// Restore preferences
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
		bestScore = settings.getInt("bs", 0);
		return bestScore; }

	/**
	 * @param engine
	 * @param activity
	 * @param camera
	 * @param vbom
	 * <br><br>
	 * We use this method at beginning of game loading, to prepare Resources Manager properly,
	 * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
	 */
	public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom)
	{
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().camera = (BoundCamera)camera;
		getInstance().vbom = vbom;
	}

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------

	public static ResourcesManager getInstance()
	{
		return INSTANCE;
	}
}
