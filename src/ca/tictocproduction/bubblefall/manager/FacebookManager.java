package ca.tictocproduction.bubblefall.manager;

import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

public final class FacebookManager {
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	public static final String GAME_NAME = "Bubble Fall";
	
	private static String sFirstName;
	private static boolean sUserLoggedIn;

	private static Session openActiveSession(final Activity pActivity, final boolean pAllowLoginUI, final StatusCallback pCallback, final List<String> pPermissions) {
		final OpenRequest openRequest = new OpenRequest(pActivity).setPermissions(pPermissions).setCallback(pCallback);
		openRequest.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
		final Session session = new Builder(pActivity.getBaseContext()).build();
		if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || pAllowLoginUI) {
			Session.setActiveSession(session);
			session.openForPublish(openRequest);
			return session;
		}
		return null;
	}
	
	public static void checkUserLoggedIn() {
		ResourcesManager.getInstance().activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				openActiveSession(ResourcesManager.getInstance().activity, false, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (session.isOpened()) {
							Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user, Response response) {
									if (user != null) {
										sFirstName = user.getFirstName();
										FacebookManager.sUserLoggedIn = true;
									} else {
										FacebookManager.sUserLoggedIn = false;
									}
								}
							});
						}
					}
				}, PERMISSIONS);
			}
		});
	}
	
	private static void loginAndPost(final String pData) {
		ResourcesManager.getInstance().activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				openActiveSession(ResourcesManager.getInstance().activity, true, new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state, Exception exception) {
						if (session.isOpened()) {
							Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user, Response response) {
									if (user != null) {
										sFirstName = user.getFirstName();
										final Session.OpenRequest openRequest;
										openRequest = new Session.OpenRequest(ResourcesManager.getInstance().activity);
										openRequest.setPermissions(PERMISSIONS);
										post(user.getFirstName(), pData);
									} else {
										sUserLoggedIn = false;
									}
								}
							});
						}
					}
				}, PERMISSIONS);
			}
		});
	}
	
	private static void post(final String pFirstName, final String pData) {
		ResourcesManager.getInstance().activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Bundle params = new Bundle();
				params.putString("name", pFirstName + pData);
				params.putString("caption", "My Game is a game for Android!");
				params.putString("description", "Click on Get button to get it!");
				params.putString("link", "https://www.google.ca/");
				//params.putString("picture", "https://linkToYourImage");
				JSONObject actions = new JSONObject();
				try {
					actions.put("name", "Get");
					actions.put("link", "http://play.google.com/store/apps/details?id=yourPackageName");
				} catch (Exception e) {};
				params.putString("actions", actions.toString());
				Request.Callback callback = new Request.Callback() {
					@Override
					public void onCompleted(Response response) {
						try {
							JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
							@SuppressWarnings("unused")
							String postID = null;
							try {
								postID = graphResponse.getString("id");
							} catch (JSONException e) {}
						} catch (NullPointerException e) {
						}
					}
				};
				Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST, callback);
				RequestAsyncTask task = new RequestAsyncTask(request);
				task.execute();
			}
		});
	}
	
	public static void postScore(final String pScore, final String pGameMode) {
		if (sUserLoggedIn) {
			post(sFirstName, " has won " + pScore + " points in " + GAME_NAME);
		} else {
			loginAndPost(" has won " + pScore + " in " + GAME_NAME);
		}
	}
	
	public static void postLevelCompletion(final String pLevel, final String pGameMode) {
		if (sUserLoggedIn) {
			post(sFirstName, " has completed Level " + pLevel + " in " + pGameMode + " in " + GAME_NAME);
		} else {
			loginAndPost(" has completed Level " + pLevel + " in " + pGameMode + " in " + GAME_NAME);
		}
	}
}