package ca.tictocproduction.bubblefall.manager;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.AvoidXfermode.Mode;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;

public class FacebookShare {
	private static final String PERMISSION = "publish_actions";
	private static final String FILE_NAME = "BubbleFreeFallScreenshot";
	private enum PendingAction {
		NONE,
		POST_PHOTO,
	}

	private static FacebookShare INSTANCE;
	private PendingAction pendingAction = PendingAction.NONE;
	private Activity activity;
	public UiLifecycleHelper uiHelper;
	private LoginButton loginButton;

	public Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
			if (session.getState() == SessionState.OPENED )
				performPublish(PendingAction.POST_PHOTO, FacebookDialog.canPresentShareDialog(activity,
						FacebookDialog.ShareDialogFeature.PHOTOS));
		}
	};

	public FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};


	private FacebookShare(){

	}

	public static FacebookShare getInstance()
	{
		if (INSTANCE == null){
			INSTANCE = new FacebookShare();
		}
		return INSTANCE;
	}

	public void createUiHelper(Activity activity){
		this.activity = activity;
		uiHelper = new UiLifecycleHelper(activity, callback);
	}

	public void onClickPostPhoto() {
		Log.d("OnClickPostPhoto", "Start");
		// Take screenshot
		savePic(takeScreenShot(activity), FILE_NAME);
		
		
		// Hack with fake login button
		loginButton = new LoginButton(activity); 
		loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {

			}
		});

		Session s = Session.getActiveSession();
		Log.d("onClickPostPhoto: ", s.toString());
		if (s == null || s.getState() != SessionState.OPENED)
		{
			Log.d("onClickPostPhoto - in if: ", s.toString());
			loginButton.performClick();
		}
		//loginButton.performClick();

		s = Session.getActiveSession();
		if (s.getState() == SessionState.OPENED){
			Log.d("OnClickPostPhoto", "after login");
			performPublish(PendingAction.POST_PHOTO, FacebookDialog.canPresentShareDialog(activity,
					FacebookDialog.ShareDialogFeature.PHOTOS));
			Log.d("OnClickPostPhoto", "End");
		}
	}

	public void onSessionStateChange(Session session, 
			SessionState state, Exception exception) {
		Log.d("onSessionStateChange", "Start");
		Log.d("onSessionStateChange - pendingAction:", "" + pendingAction);
		Log.d("onSessionStateChange - state: ", "" + state);
		if (pendingAction != PendingAction.NONE &&
				(exception instanceof FacebookOperationCanceledException ||
						exception instanceof FacebookAuthorizationException)) {
			Log.d("onSessionStateChange", "in If");
			new AlertDialog.Builder(activity)
			.setTitle("Cancelled")
			.setMessage("Unable to perform selected action because permissions were not granted.")
			.setPositiveButton("OK", null)
			.show();
			pendingAction = PendingAction.NONE;
		} else if (state == SessionState.OPENED_TOKEN_UPDATED) {
			Log.d("onSessionStateChange", "in else");
			handlePendingAction();
		}
		//updateUI();
		Session s = Session.getActiveSession();
		Log.d("onSessionStateChange - before end: ", s.toString());
		Log.d("onSessionStateChange", "End");
	}

	private void performPublish(PendingAction action, boolean allowNoSession) {
		Session session = Session.getActiveSession();
		Log.d("performPublish", "start");
		if (session != null) {
			Log.d("performPublish", "session != null");
			pendingAction = action;
			if (hasPublishPermission()) {
				Log.d("performPublish", "hasPublishPermission");
				// We can do the action right away.
				handlePendingAction();
				return;
			} else if (session.isOpened()) {
				Log.d("performPublish", "session is opened");
				// We need to get new permissions, then complete the action when we get called back.
				session.requestNewPublishPermissions(new Session.NewPermissionsRequest(activity, PERMISSION));
				return;
			}
		}

		if (allowNoSession) {
			Log.d("performPublish", "allow no session");
			pendingAction = action;
			handlePendingAction();
		}
		Log.d("performPublish", "end");
	}

	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		Log.d("handlePendingAction", "Start");
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction) {
		case POST_PHOTO:
			postPhoto();
			break;
		}
		Log.d("handlePendingAction", "End");
	}

	private void postPhoto() {
		Log.d("Post Photo", "postPhoto get called");
		Bitmap image = loadPhoto();
		if (image == null) {
			Log.d("postPhoto: ", "Image bitmap = null");
			return;
		}
		//Bitmap image = BitmapFactory.decodeFile("file:///android_asset/gfx/splash.jpg");
		//Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.icon);
		if (FacebookDialog.canPresentShareDialog(activity,
				FacebookDialog.ShareDialogFeature.PHOTOS)) {
			Log.d("Post Photo", "In if");
			FacebookDialog shareDialog = createShareDialogBuilderForPhoto(image).build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		}else if (hasPublishPermission()) {
			Log.d("Post Photo", "In else");
			/* TEMPORARY REMOVE IT FOR TESTING PURPOSE */
			Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), image, new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					String title = "";
					String alertMessage = "";

					if (response.getError() == null){
						title = "Successfully posted";
						alertMessage = "Successfully posted image to facebook";
					} else {
						title = "Error";
						alertMessage = "Can't post image to facebook" + response.getError().getErrorMessage();
					}

					new AlertDialog.Builder(FacebookShare.this.activity)
					.setTitle(title)
					.setMessage(alertMessage)
					.setPositiveButton("OK", null)
					.show();
				}
			});
			request.executeAsync();
			
		}
		Log.d("Post Photo", "postPhoto end");
	}

	private Bitmap loadPhoto() {
		// load image
		try {
			// get input stream
			InputStream ims = activity. openFileInput(FILE_NAME);
			if (ims == null)
				Log.d("loadPhoto", "ims null");
			return BitmapFactory.decodeStream(ims);
		}
		catch(IOException ex) {
			Log.e("loadphoto: ", "Exception: " + ex.getMessage());
		}
		return null;
	}
	
	private Bitmap takeScreenShot(Activity activity)
	{
	    View view = activity.getWindow().getDecorView();
	    view.setDrawingCacheEnabled(true);
	    view.buildDrawingCache();
	    Bitmap b1 = view.getDrawingCache();
	    Rect frame = new Rect();
	    activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
	    int statusBarHeight = frame.top;
	    int width = activity.getWindowManager().getDefaultDisplay().getWidth();
	    int height = activity.getWindowManager().getDefaultDisplay().getHeight();

	    Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height  - statusBarHeight);
	    view.destroyDrawingCache();
	    return b;
	}
	
	private void savePic(Bitmap b, String strFileName)
	{
	    FileOutputStream fos = null;
	    try
	    {
	        fos = activity.openFileOutput(strFileName, Context.MODE_PRIVATE);
	        if (null != fos)
	        {
	            b.compress(Bitmap.CompressFormat.PNG, 90, fos);
	            fos.flush();
	            fos.close();
	        }
	    }
	    catch (FileNotFoundException e)
	    {
	        e.printStackTrace();
	    }
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	}

	private FacebookDialog.PhotoShareDialogBuilder createShareDialogBuilderForPhoto(Bitmap... photos) {
		return new FacebookDialog.PhotoShareDialogBuilder(activity)
		.addPhotos(Arrays.asList(photos));
	}

	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		Log.d("hasPublishPermission", session.isOpened() + "");
		return session != null && session.getPermissions().contains(PERMISSION);
	}
}
