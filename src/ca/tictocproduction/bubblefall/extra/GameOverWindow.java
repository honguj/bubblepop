package ca.tictocproduction.bubblefall.extra;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;

import android.app.Activity;
import ca.tictocproduction.bubblefall.manager.ResourcesManager;


public class GameOverWindow extends Sprite{

	private static final float SCORE_TEXT_ALIGN_LEFT = 60;
	private static final float WIDTH = 300;
	private static final float HEIGHT = 400;
	
	private Text scoreText;
	private Text bestScoreText;

	public GameOverWindow(VertexBufferObjectManager vbom)
	{
		super(0, 0, HEIGHT, WIDTH, ResourcesManager.getInstance().gameover_window_region, vbom);
	}
	
	public void display(int score, Scene scene, Camera camera,
			VertexBufferObjectManager vbom){
		// CREATE SCORE TEXT
		scoreText = new Text(SCORE_TEXT_ALIGN_LEFT, 
				camera.getCenterY() + 10, 
				ResourcesManager.getInstance().font, 
				"Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setAnchorCenter(0, 0);    
		scoreText.setText("Score: " + score);

		// Save new score
		ResourcesManager.getInstance().saveNewScore(score);
		
		// CREATE BEST SCORE
		bestScoreText = new Text(SCORE_TEXT_ALIGN_LEFT,
					camera.getCenterY() - 80,
					ResourcesManager.getInstance().font,
					"Best Score: 0123456789",
					new TextOptions(HorizontalAlign.LEFT), vbom);
		bestScoreText.setAnchorCenter(0, 0);
		bestScoreText.setText("Best score: " + ResourcesManager.getInstance().getBestScore());

		//Attach our window panel 
		this.detachSelf();
		setPosition(camera.getCenterX(), camera.getCenterY());
		scene.attachChild(this);
		scene.attachChild(scoreText);
		scene.attachChild(bestScoreText);
	}
	
	public void hide(){
		this.detachSelf();
		scoreText.detachSelf();
		bestScoreText.detachSelf();
		scoreText = null;
		bestScoreText = null;
	}

}
