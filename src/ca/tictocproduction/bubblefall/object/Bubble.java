package ca.tictocproduction.bubblefall.object;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import ca.tictocproduction.bubblefall.manager.ResourcesManager;
import ca.tictocproduction.bubblefall.scene.GameScene;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Bubble extends AnimatedSprite{

	private Body body;
	private int lastPos = 0;
	
	public Bubble(float pX, float pY, VertexBufferObjectManager vbo, Camera camera, PhysicsWorld physicsWorld){
		super(pX, pY, ResourcesManager.getInstance().bubble_region, vbo);
		createPhysics(camera, physicsWorld);
		lastPos = (int)getY();
	}
	
	public abstract void onDie();
	public abstract void scoring();
	
	private void createPhysics(final Camera camera, PhysicsWorld physicsWorld){
		//body = PhysicsFactory.createCircleBody(physicsWorld, this, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 0));
		body = PhysicsFactory.createBoxBody(physicsWorld, GameScene.BUBBLE_INIT_X, 
				GameScene.BUBBLE_INIT_Y, 40-1, 40-1, 0, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 0));
	    body.setUserData("bubble");
	    body.setFixedRotation(true);
	    
	    physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, false)
	    {
	        @Override
	        public void onUpdate(float pSecondsElapsed)
	        {
	            super.onUpdate(pSecondsElapsed);
	            camera.onUpdate(0.1f);
	            
	            if (getY() <= 0 || getY() >= camera.getHeight())
	            {                    
	                onDie();
	            }
	            else if (getFallingDistance() >= 60){
	            	scoring();
	            }
	        }
	    });
	    
	    //final long[] BUBBLE_ANIMATE = new long[] { 100, 100, 100 };
	    //animate(BUBBLE_ANIMATE, 0, 0, true);
	}
	
	public void reinit(){
		this.setX(GameScene.BUBBLE_INIT_X);
		this.setY(GameScene.BUBBLE_INIT_Y);
		this.setVisible(true);
	}
	
	public void jump()
	{
		lastPos = (int)this.getY();
		ResourcesManager.getInstance().bubbleJump.play();
	    body.setLinearVelocity(new Vector2(body.getLinearVelocity().x, 6));
	}
	
	public float getFallingDistance() { return Math.abs(this.getY() - lastPos); }
	
}
