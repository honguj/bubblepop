package ca.tictocproduction.bubblefall.object;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Bird extends AnimatedSprite{
	public static final float VELOCITY_X = 6;

	public static final int LEFT_DIRECTION = -1;
	public static final int RIGHT_DIRECTION = 1;
	private static final int LEFT_TILE_INDEX = 0;
	private static final int RIGHT_TILE_INDEX = 3;

	private Body body;
	private float posInitialX;
	private int direction;
	private int tileIndex;

	public Bird(float pX, float pY, VertexBufferObjectManager vbo, Camera camera, PhysicsWorld physicsWorld,
			ITiledTextureRegion textureRegion){
		super(pX, pY, textureRegion, vbo);
		posInitialX = pX;
		if (posInitialX < 240){
			direction = RIGHT_DIRECTION;
			tileIndex = RIGHT_TILE_INDEX;
		}
		else{
			direction = LEFT_DIRECTION;
			tileIndex = LEFT_TILE_INDEX;
		}
		createPhysics(camera, physicsWorld);
	}

	public abstract void onDie();

	private void createPhysics(final Camera camera, PhysicsWorld physicsWorld){
		body = PhysicsFactory.createBoxBody(physicsWorld, this, BodyType.KinematicBody, PhysicsFactory.createFixtureDef(0, 0, 0));

		body.setUserData("bird");
		body.setFixedRotation(true);

		physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, false)
		{
			@Override
			public void onUpdate(float pSecondsElapsed)
			{
				super.onUpdate(pSecondsElapsed);
				camera.onUpdate(0.1f);

				if (getX() <= -20 || getX() >= 500)
				{                    
					onDie();
				}

			}
		});

		final long[] BIRD_ANIMATE = new long[] { 100, 100, 100 };
		animate(BIRD_ANIMATE, tileIndex, tileIndex + 2, true);
	}

	public void move(){
		float lower = (float) 0.2;
		float upper = (float) 0.8;
		float random = (float)Math.random() * (upper - lower) + lower;
		body.setLinearVelocity(new Vector2(direction * VELOCITY_X * random, 0));
	}

}
